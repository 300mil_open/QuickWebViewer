/*
https://gitlab.com/300000-kms/QuickWebViewer
*/

let ts = '?v=' + new Date().getTime()

var app = new Vue({
    el: '#app',
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdi',
        },
        theme: {
            themes: {
                light: {
                    primary: '#e32122',
                },
            },
        },
    }),

    data: function () {
        return {
            confData: [
                //{ 'n': 'conf', 'u': 'conf.json' + ts },
                {
                    'n': 'conf',
                    'u': 'conf.json' + ts
                },
            ],
            c_data: {},
            ready: false,
            loading: false,
            currentChapter: -1,
            currentMap: 0,
            type: 'intro',
            maps: {},
            map: null,
            layers: [],
            posMenu: 0,
            menuControls: [],

            atlas_menu: true,
            atlas_legend: [],

            url_project: 'data/',
            status_menu: 'close', // layers, description, close

            page: 0,
            numPages: 0,

            baseurl: window.location.href.split('/').slice(0, -1).join('/'),
            debug_styles: false,
            server_test: 'https://sandbox.300000.eu/conakry/',
            xz:2,
            dialog:false,
            measure_options: {
                lang: {
                    areaMeasurementButtonTitle: 'Measure area',
                    lengthMeasurementButtonTitle: 'Measure length',
                    clearMeasurementsButtonTitle: 'Clear measurements',
                },
                units: 'metric',
                style: {
                    text: {
                        radialOffset: 0.9,
                        letterSpacing: 0.05,
                        color: '#000',
                        haloColor: '#fff',
                        haloWidth: 2,
                    },
                    common: {
                        midPointRadius: 4,
                        midPointColor: '#D20C0C',
                        midPointHaloRadius: 5,
                        midPointHaloColor: '#FFF',
                    },
                    areaMeasurement: {
                        fillColor: '#D20C0C',
                        fillOutlineColor: '#000',
                        fillOpacity: 0.01,
                        lineWidth: 2,
                    },
                    lengthMeasurement: {
                        lineWidth: 2,
                        lineColor: "#000000",
                    },
                }
            }
        }
    },

    methods: {

        changeStatus(s) {
            this.status_menu = s;

            this.$nextTick(() => {
                this.resize;
            })

        },


        resize: function () {
            if (this.map) {
                this.map.resize();
            }
        },


        fillMetadata: function (r) {
            for (c in r['chapters']) {
                if (r['chapters'][c]['maps'] != undefined) {
                    r['chapters'][c]['maps'].forEach((map) => {
                        map.layers.forEach((layer) => {
                            layer.maplibreLayers.forEach((l) => {
                                if (l.metadata == undefined) {
                                    l['metadata'] = {}
                                };
                                //legend_style = l.paint; //copy
                                // legend_style = { ...l.paint };
                                // console.log(l)
                                if (l.paint!=undefined){
                                    legend_style = JSON.parse(JSON.stringify(l.paint))
                                    if (legend_style['line-width'] != undefined) {
                                        if (typeof (legend_style['line-width']) == 'string' && legend_style['line-width'].indexOf('m') != -1) {
                                            legend_style['line-units'] = 'meters';
                                            legend_style['line-width'] = parseInt(legend_style['line-width'].replace('m', ''));
                                        }
                                    }

                                    if (legend_style['circle-stroke-width'] != undefined) {
                                        if (typeof (legend_style['circle-stroke-width']) == 'string' && legend_style['circle-stroke-width'].indexOf('m') != -1) {
                                            legend_style['circle-units'] = 'meters';
                                            legend_style['circle-stroke-width'] = parseInt(legend_style['circle-stroke-width'].replace('m', ''));
                                        }
                                    }

                                    l.metadata['paint'] = legend_style;
                                }
                            
                                l.metadata['name'] = layer.name;
                                if (layer.legend && layer.legend.legend_status == true) {
                                    l.metadata['legend_status'] = true;
                                } else {
                                    l.metadata['legend_status'] = false;
                                }
                            })
                        })
                    })
                }
            }
            return r
        },


        checkStyles: function (r) {
            // console.log(this.baseurl, this.server_test)
            if (this.baseurl == 'http://127.0.0.1:5500' || this.baseurl == 'http://localhost:5500') {
                this.baseurl = this.server_test;
            } else {
                this.baseurl = this.baseurl + '/'
            }
            // console.log(this.baseurl, this.server_test)
            // this.baseurl = this.baseurl
            // console.log(this.baseurl)

            for (c in r['chapters']) {
                if (r['chapters'][c]['maps'] != undefined) {

                    r['chapters'][c]['maps'].forEach((map) => {
                        map.layers.forEach((layer) => {
                            let tiles = [];
                            layer.source.tiles.forEach((ss) => {
                                if (ss.indexOf('{baseurl}') != -1) {
                                    tiles.push(ss.replace('{baseurl}', this.baseurl))
                                } else {
                                    tiles.push(ss)
                                }
                            })
                            layer.source.tiles = tiles;
                        })
                    })

                    r['chapters'][c]['maps'].forEach((map) => {
                        let minz = map.minzoom;
                        let maxz = map.maxzoom;
                        map.layers.forEach((layer) => {
                            let types = [];
                            layer.maplibreLayers.forEach((l) => {
                                types.push(l.type);
                            })
                            layer.maplibreLayers.forEach((l) => {

                                if (l.paint && l.paint['line-width'] != undefined) {
                                    if (typeof (l.paint['line-width']) == 'string' && l.paint['line-width'].indexOf('m') != -1) {
                                        let val = parseFloat(l.paint['line-width'].replace('m', ''))

                                        l.paint['line-width'] = [
                                            "interpolate",
                                            ["exponential", 2],
                                            ["zoom"],
                                            minz, val * Math.pow(2, minz) /50000,
                                            maxz+this.xz, val * Math.pow(2, (maxz+this.xz)) / 50000

                                        ];
                                    }
                                }

                                if (l.paint && l.paint['fill-opacity'] && l.paint['fill-color'] == undefined) {
                                    l.paint['fill-opacity'] = 0;
                                }

                                /// labels
                                if (l.paint && l.paint['text-halo-width'] && typeof (l.paint['text-halo-width']) == 'string') {
                                    l.paint['text-halo-width'] = Number(l.paint['text-halo-width']) / 2
                                }
                                if (l.paint && l.layout && l.layout['text-size'] && typeof (l.layout['text-size']) == 'number') {
                                    l.layout['text-size'] = l.layout['text-size']
                                }

                                //check text offset
                                if (l.layout != undefined && types.indexOf('fill') == -1 && types.indexOf('line') != -1) {
                                    l.layout['symbol-placement'] = 'line';
                                }

                                if (l.layout != undefined && l.layout['text-offset'] != undefined) {
                                    if (typeof (l.layout['text-offset']) != 'object') {
                                        l.layout['text-offset'] = [l.layout['text-offset'], l.layout['text-offset']]
                                    }
                                    if (this.debug_styles) {
                                        l.layout = {}
                                    }
                                }

                                // if (l.layout && l.layout['text-size'] && typeof (l.layout['text-size']) == 'string') {
                                //     //l.layout['text-size'] = Number(l.layout['text-size']);

                                //     let val = parseInt(l.layout['text-size'].replace('m', ''))
                                //         l.layout['text-size'] = [
                                //             "interpolate",
                                //             ["exponential", 2],
                                //             ["zoom"],
                                //             layer.minzoom, val / (layer.maxzoom - layer.minzoom) *1,
                                //             layer.maxzoom, val *1,
                                //         ];
                                // }

                                if (l.paint && l.paint['line-dasharray'] && typeof (l.paint['line-dasharray']) == 'string') {
                                    l.paint['line-dasharray'] = l.paint['line-dasharray'].split(' ');
                                    l.paint['line-dasharray'] = l.paint['line-dasharray'].map((x) => Number(x));
                                }

                                if (l.paint && l.paint['line-dasharray']) {
                                    l.paint['line-dasharray'] = l.paint['line-dasharray'].map((x) => x / 4);
                                }

                                if (l.paint && l.paint['circle-radius'] && typeof (l.paint['circle-radius']) == 'string' && l.paint['circle-radius'].indexOf('m') != -1){
                                    let val = parseFloat(l.paint['circle-radius'].replace('m', ''))
                                    l.paint['circle-radius'] = [
                                        "interpolate",
                                        ["exponential",2],
                                        ["zoom"],
                                        minz,   val * Math.pow(2, minz) /80000 ,
                                        maxz+this.xz, val * Math.pow(2, (maxz+this.xz))/80000
                                        // minz,   val * (minz) * (minz)/5000 ,
                                        // maxz+this.xz, val * (maxz+this.xz)* (maxz+this.xz)/500
                                    ];

                                    // console.log(minz, maxz)
                                    // console.log( l.paint['circle-radius'])
                                }

                                if (l.paint && l.paint['circle-stroke-width'] && typeof (l.paint['circle-stroke-width']) == 'string' && l.paint['circle-stroke-width'].indexOf('m') != -1){
                                    let val = parseFloat(l.paint['circle-stroke-width'].replace('m', ''))

                                        l.paint['circle-stroke-width'] = [
                                            "interpolate",
                                            ["exponential", 2],
                                            ["zoom"],
                                            minz,   val * Math.pow(2, minz) /80000 ,
                                            maxz+this.xz, val * Math.pow(2, (maxz+this.xz))/80000
                                        ];
                                        //l.paint['circle-stroke-width']=0
                                }

                                if (this.debug_styles) {
                                    if (l['type'] == 'fill') {
                                        l['paint'] = {
                                            'fill-color': 'red'
                                        }
                                    } 
                                    else if (l['type'] == 'line') {
                                        l['paint'] = {
                                            'line-color': 'blue'
                                        }
                                    } 
                                    else {
                                        l['paint'] = {
                                            "text-color": "green"
                                        }
                                    }
                                }

                            })
                        })
                    })
                }
            }

            return r
        },


        prepareData: function () {
            for (const c in this.confData) {
                fetch(this.url_project + this.confData[c]['u'])
                    .then(res => res.json())
                    .then(r => {

                        if (this.confData[c]['n'] == 'conf') {
                            //r = this.putids(r)
                            r = this.fillMetadata(r)
                            r = this.checkStyles(r)
                            this.c_data[this.confData[c]['n']] = r;
                        } else {
                            this.c_data[this.confData[c]['n']] = r;
                        }
                        if (Object.keys(this.c_data).length == this.confData.length) {
                            this.ready = true
                            this.loading = true
                        }
                    })
            }
        },


        loadChapter: function (c) {

            let el = document.getElementById('mycontent');
            if (c == -1) {
                this.type = 'intro';
                this.currentChapter = c;
            } 
            else if (this.c_data.conf.chapters[c].type == 'md') { //markdown
                this.currentChapter = c;
                this.type = 'md';
            } 
            else if (this.c_data.conf.chapters[c].type == 'pdf') { //pdf viewer
                this.currentChapter = c;
                this.type = 'pdf';
                this.pdf = this.c_data.conf.chapters[c].pdf;
            } 
            else if (this.c_data.conf.chapters[c].type == 'atlas') { //atlas
                this.currentChapter = c;
                this.type = 'atlas';
                this.maps = this.c_data.conf.chapters[c].maps;
                this.backgrounds = this.c_data.conf.chapters[c].atlas_background;

                this.currentMap = Object.keys(this.c_data.conf.chapters[c].maps)[0];
                this.layers = [];

                this.makeMapLibre(c).then(() => {
                    this.loadMapLibre(Object.keys(this.maps)[0]);
                })
            }
        },


        makeMapLibre: function (c) {

            return new Promise(resolve => {
                this.$nextTick(() => {

                    let sources = {},
                        layers = [{
                            "id": "background",
                            "type": "background",
                            "paint": {
                                "background-color": "#FFFFFF",
                            }
                        }];

                    for (index in this.backgrounds) {
                        let bg = this.backgrounds[index];
                    
                        if (bg.name === "OpenStreetMap") {
                            sources[bg.name] = {
                                "type": "raster",
                                "tiles": [bg.url],
                                "tileSize": 256,
                                "attribution": "&copy; OpenStreetMap Contributors",
                                "maxzoom": 19
                            };
                            layers.push({
                                "id": bg.name,
                                "type": "raster",
                                "source": bg.name
                            });
                        }
                        else if (bg.name === "Satellite") {
                            sources[bg.name] = {
                                "type": "raster",
                                "tiles": [bg.url],
                                "tileSize": 256,
                                "attribution": "&copy; Sentinel-2 cloudless 2022 by EOX IT Services GmbH",
                                "maxzoom": 19
                            };
                            layers.push({
                                "id": bg.name,
                                "type": "raster",
                                "source": bg.name
                            });
                        }
                    }

                    let style = {
                        "version": 8,
                        "glyphs": "https://demotiles.maplibre.org/font/{fontstack}/{range}.pbf",
                        "sources": sources,
                        "layers": layers
                    }

                    let el = document.getElementById('atlas-map');
                    el.innerHTML = '';
                    this.map = new maplibregl.Map({
                        container: 'atlas-map',
                        center: [0, 0],
                        maxZoom: 15,
                        minZoom: 8,
                        zoom: 15,
                        style: style
                    });

                    //*********************** */
                    this.map.dragRotate.disable();
                    this.map.touchZoomRotate.disableRotation();
                    let scale = new maplibregl.ScaleControl({
                        maxWidth: 250,
                        unit: 'metric'
                    });

                    this.map.addControl(scale);

                    class HomeButton {
                        onAdd(map) {
                            const div = document.createElement("div");
                            div.id = "info-coordinates";
                            div.addEventListener("contextmenu", (e) => e.preventDefault());
                            return div;
                        }
                    }

                    const homeButton = new HomeButton();
                    this.map.addControl(homeButton, "bottom-left");

                    this.map.on('style.load', () => {
                        resolve();
                    });

                    // Add zoom and rotation controls to the map.
                    this.map.addControl(new maplibregl.NavigationControl());

                    // coordinates
                    this.map.on('mousemove', (e) => {
                        document.getElementById('info-coordinates').innerHTML =
                            'lat: ' + Number(e.lngLat.wrap().lat).toFixed(5) +
                            ' | lng: ' + Number(e.lngLat.wrap().lng).toFixed(5);
                    })

                    ////// measures
                    this.map.addControl(new maplibreGLMeasures.default(this.measure_options), 'top-left');

                    // backgrounds
                    if (this.backgrounds.length > 0) {
                        let allBGnames = [];
                        for (bg in this.backgrounds) {
                            allBGnames.push(this.backgrounds[bg].name);
                        }

                        this.map.addControl(new MenuControl({
                            "name": "No background"
                        }, allBGnames, true), 'top-left');
                        this.posMenu++;
                        for (bg in this.backgrounds) {
                            this.map.addControl(new MenuControl(this.backgrounds[bg], allBGnames, this.posMenu === 0), 'top-left');
                            this.posMenu++;
                        }
                    }

                    //popup
                    const popup = new maplibregl.Popup({
                        closeButton: true,
                        closeOnClick: true
                    });

                    this.map.on('click', (e) => {

                        let mapContainer = document.getElementById('atlas-map');
                        let measureActive = mapContainer.classList.contains('mode-draw_line_string') || mapContainer.classList.contains('mode-draw_polygon');

                        if (!measureActive) {

                            let features = this.map.queryRenderedFeatures(e.point);
                            features = features.filter((f) => f.layer.metadata.legend_status == true);

                            features = features.filter((f) => {
                                var desc = f.layer.id.split(':')[2];
                                var lab =  desc.slice((desc.indexOf(')') + 1))
                                if (lab == 'labeling'){
                                    return false
                                }else{
                                    return true
                                }
                            });
                            
                            // console.log(features.length)

                            if (features.length > 0) {
                                if (features[0].layer.metadata && features[0].layer.metadata.name) {
                                    let name = features[0].layer.metadata.name
                                    let description = features[0].layer.id.split(':')[2]

                                    labeling = description.slice((description.indexOf(')') + 1))
                                    let txt = `<b>${name}</b>`
                                    if (description != '' && labeling != 'labeling') {
                                        txt += '<br>' + labeling
                                    }

                                    this.map.flyTo({
                                        center: e.lngLat,
                                        essential: true,
                                        zoom: this.map.getMaxZoom() - 1
                                    });

                                    this.map.once('moveend',()=>{
                                        popup.setLngLat(e.lngLat).setHTML(txt).addTo(this.map);
                                    }) 
                                    
                                }
                            }
                        }
                    });

                    // this.map.on('zoom', () => {
                    //     const currentZoom = this.map.getZoom();
                    //     console.log(currentZoom)
                    // });
                })
            })
        },


        loadMapLibre: function (m, c) {

            //allstyles = {};

            this.$nextTick(() => {

                // clean map
                this.atlas_legend = [];
                this.cleanMap();

                this.layers = [];

                this.menuControls.forEach((control) => {
                    this.map.removeControl(control);
                });
                this.menuControls = [];
                this.posMenu = 0;

                //set current map
                this.currentMap = m;
                this.map.setMinZoom(this.maps[m].minzoom);
                this.map.setMaxZoom(this.maps[m].maxzoom + this.xz);
                this.map.setMaxZoom(this.maps[m].maxzoom + this.xz);
                // this.map.setCenter(this.c_data.conf.chapters[this.currentChapter]['view.center']);
                this.map.setMaxBounds(this.c_data.conf.chapters[this.currentChapter]['view.bbox']);
                this.map.fitBounds(this.c_data.conf.chapters[this.currentChapter]['view.bbox'], {linear:false, duration:0});

                // get menu layer ids
                let menuLayerIds = [];
                for (let mm in this.maps[m].layers) {
                    let layer = this.maps[m]['layers'][mm];
                    if (layer.type == 'maplibre' && layer.layer_id !== undefined && layer.menu === 'radio') {
                        menuLayerIds.push(layer.layer_id);
                    }
                }

                //load layers
                for (let mm in this.maps[m].layers) {
                    let layer = this.maps[m]['layers'][mm];
                    if (layer.type == 'maplibre') {
                        if (layer.layer_id !== undefined)
                            this.loadMapLibreLayer(m, mm, menuLayerIds);
                        else
                            console.log('!!!layer_id is undefined')
                    }
                }

                this.atlas_legend.reverse();

                /**/

                // if (this.c_data.conf.chapters[this.currentChapter].template != undefined) {
                //     let template = Handlebars.compile(this.c_data.conf.chapters[this.currentChapter].template);
                //     this.c_data.conf.chapters[this.currentChapter].template_formatted = template(this.maps[m].properties)
                // } else {
                //     this.c_data.conf.chapters[this.currentChapter].template_formatted = '';
                // }

                this.$forceUpdate();

                // hide backgrounds initially
                for (index in this.backgrounds) {
                    this.map.setLayoutProperty(this.backgrounds[bg].name, 'visibility', 'none');
                }
            })
        },


        cleanMap: function () {
            this.layers.forEach((layer) => {
                layer.maplibreLayers.forEach((capa) => {
                    if (this.map.getLayer(capa.id))
                        this.map.removeLayer(capa.id);
                });
                if (this.map.getSource(layer.layer_id))
                    this.map.removeSource(layer.layer_id);
            });
        },


        // loadLayer: function () {},


        loadMapLibreLayer: function (m, mm, menuLayerIds) {

            let layer = this.maps[m].layers[mm];

            // preprocess and add maplibre source
            if (layer.source !== null && layer.source !== undefined) {
                // add project path to geojson data source
                if (layer.source.type === 'geojson' && layer.source.data.indexOf(this.url_project) === -1) {
                    layer.source.data = this.url_project + layer.source.data;
                }

                // cache
                let cache = true;
                if (layer.cache !== null && layer.cache !== undefined) {
                    cache = layer.cache;

                    if (!cache) {
                        // add timestamp
                        if (layer.source.type === 'vector') {
                            layer.source.tiles.forEach((tile, i) => {
                                layer.source.tiles[i] += "?ts=" + new Date().valueOf();
                            });
                        } else if (layer.source.type === 'geojson') {
                            layer.source.data += "?ts=" + new Date().valueOf();
                        }
                    }
                }

                // subdomain url template
                if (layer.source.type === 'vector') {
                    let newTileUrls = [];
                    layer.source.tiles.forEach((tile, i) => {
                        const regex = /({[0-9][-][0-9]})/,
                            regex1 = /({[0-9][-])/,
                            regex2 = /([-][0-9]})/;
                        if (regex.test(tile)) {
                            // substitute subdomain patterns {0-5}
                            const pos1 = tile.search(regex1),
                                pos2 = tile.search(regex2);
                            const num1 = parseInt(tile[pos1 + 1]),
                                num2 = parseInt(tile[pos2 + 1]);
                            for (let i = num1; i <= num2; i++) {
                                let newTileUrl = tile.replace(regex, i);
                                newTileUrls.push(newTileUrl);
                            }
                        } else {
                            // no subdomain pattern to substitute
                            newTileUrls.push(tile);
                        }
                    });
                    layer.source.tiles = newTileUrls;
                }

                //console.log('1. add source', layer.layer_id);
                this.map.addSource(layer.layer_id, layer.source);

                // preprocess and add maplibre style
                if (layer.maplibreLayers !== null && layer.maplibreLayers !== undefined) {
                    this.layers.push(layer);

                    // layer style
                    if (layer.menu === 'radio' && this.posMenu === 0 || !layer.hasOwnProperty('menu')) {

                        layer.maplibreLayers.forEach((l) => {
                            if (l.filter !== "ELSE") {
                                // console.log("add", l.id);
                                this.map.addLayer(l);
                            }
                        })
                    }

                    // layer menu
                    if (layer.menu === 'radio') {
                        let menuControl = new MenuControl(layer, this.posMenu === 0, menuLayerIds);
                        this.map.addControl(menuControl, 'top-left');
                        this.menuControls.push(menuControl);
                        this.posMenu++;
                    }

                    // legend
                    if (layer.legend && layer.legend.legend_status) {
                        // console.log('LEGEND')
                        // this.atlas_legend.push('<h6 class="layer-name">' + layer.name + '</h6>');
                        // // }
                        // this.atlas_legend.push(this.createSVGLegend(layer.maplibreLayers));

                        console.log( layer.name );
                        var div = '<div>'+'<h6 class="layer-name">' + layer.name + '</h6>'+this.createSVGLegend(layer.maplibreLayers)+'</div>'
                        this.atlas_legend.push(div)
                        // console.log(this.atlas_legend)
                    }
                    //reverse atlas_legend
                    

                }

                //

                // // popup
                // if (layer.popup.popup_status) {

                //     let map = this.map;
                //     this.map.on('click', layer.layer_id, function (e) {
                //         new maplibregl.Popup()
                //             .setLngLat(e.lngLat)
                //             .setHTML(layer.name + ': ' + e.features[0].properties[layer.popup.popup_value])
                //             .addTo(map);
                //     });

                //     // Change the cursor to a pointer when the mouse is over the states layer.
                //     this.map.on('mouseenter', layer.layer_id, function () {
                //         map.getCanvas().style.cursor = 'pointer';
                //     });

                //     // Change it back to a pointer when it leaves.
                //     this.map.on('mouseleave', layer.layer_id, function () {
                //         map.getCanvas().style.cursor = '';
                //     });
                // }
            }
        },


        createSVGLegend: function (styles) {
            let legend = [];
            let legend_items = {};
            styles.forEach((style) => {

                let iid = style.id.split(':').slice(0, 3).join(':')
                if (legend_items[iid] == undefined) {
                    legend_items[iid] = {
                        'styles': [],
                        'geom_type': []
                    }
                }

                //elimina el resto de los filtros
                if (style.filter != 'ELSE') {
                    legend_items[iid]['styles'].push(style)
                    geom_type = Object.keys(style.paint)[0].split('-')[0]
                    geom_type = style.type;
                    legend_items[iid]['geom_type'].push(geom_type)
                }

            });

            let symbol = ''
            for (k in legend_items) {
                let li = legend_items[k]
                if (li['geom_type'].indexOf('fill') != -1) {
                    var draw = SVG().size(50, 20);
                    li['styles'].forEach((s) => {
                        ss = s.metadata;
                        if (Object.keys(ss.paint)[0].split('-')[0] == 'fill') {
                            if (ss.paint['fill-color'] == undefined) {
                                ss.paint['fill-color'] = 'transparent';
                            }

                            rect = draw.rect(50, 20).fill(ss.paint['fill-color']);
                            if (ss.paint['fill-opacity'] != undefined) {
                                rect.attr({
                                    'fill-opacity': ss.paint['fill-opacity']
                                })
                            }

                        } else if (Object.keys(ss.paint)[0].split('-')[0] == 'line') {

                            let at = {};
                            if (ss.paint['line-width'] != undefined) {
                                at['width'] = ss.paint['line-width'] * 2;
                                if (at['width'] <2) {
                                    at['width']=2
                                }                           
                            };
                            if (ss.paint['line-color'] != undefined) {
                                at['color'] = ss.paint['line-color']
                            };
                            if (ss.paint['line-opacity'] != undefined) {
                                at['opacity'] = ss.paint['line-opacity']
                            };
                            if (ss.paint['line-dasharray'] != undefined) {
                                at['dasharray'] = ss.paint['line-dasharray'].join(' ')
                            };
                            draw.rect(50, 20).fill('none').stroke(at);
                        }
                    })

                    symbol = draw.svg();
                    label = li.styles[0].id.split(':')[2];
                    label = label.slice(label.indexOf(')') + 1) //clean label parentesis

                    let entry = '<div class="legend-row"><div class="legend-symbol-left">' + symbol + '</div><div class="legend-symbol-right">' + label + '</div></div>'
                    legend.push(entry)

                } 
                else if (li['geom_type'].indexOf('line') != -1) {
                    var draw = SVG().size(50, 20);
                    var gw = draw.group();
                    let h = 0;
                    var sc = 1;
                    //tomamos el max width
                    li['styles'].forEach((s) => {
                        ss = s.metadata;
                        if (Object.keys(ss.paint)[0].split('-')[0] == 'line') {
                            let at = {};
                            if (ss.paint['line-width'] != undefined) {
                                w = ss.paint['line-width']/sc;
                            }
                            if (w > h) {
                                h = w;
                            }
                        }
                    })
                    let f = 1;
                    if (h>10){f = 10/h}
                    li['styles'].forEach((s) => {
                        ss = s.metadata;
                        if (Object.keys(ss.paint)[0].split('-')[0] == 'line') {
                            let at = {};
                            if (ss.paint['line-width'] != undefined) {
                                at['width'] = ss.paint['line-width']*f ///sc;

                                // if (at['width'] <1) {
                                //     at['width'] = 3
                                // }   

                                // if (at['width'] > h) {
                                //     h = at['width'];
                                // }
                            };

                            if (ss.paint['line-color'] != undefined) {
                                at['color'] = ss.paint['line-color']
                            };

                            if (ss.paint['line-opacity'] != undefined) {
                                at['opacity'] = ss.paint['line-opacity']
                            };

                            if (ss.paint['line-dasharray'] != undefined) {
                                at['dasharray'] = ss.paint['line-dasharray'].join(' ')
                            };

                            gw.line(0, 0, 50, 0).stroke(at);

                        }
                    })

                    h = Math.min(h, 40)

                    draw.size(50, h)
                    gw.move(0, h / 2)

                    symbol = draw.svg();
                    label = li.styles[0].id.split(':')[2];
                    label = label.slice(label.indexOf(')') + 1) //clean label parentesis
                    let entry = '<div class="legend-row"><div class="legend-symbol-left">' + symbol + '</div><div class="legend-symbol-right">' + label + '</div></div>'
                    legend.push(entry)

                }else if (li['geom_type'].indexOf('circle') != -1) {
                    var draw = SVG().size(20, 20);
                    var gw = draw.group();
                    var sc = 10;
                    li['styles'].forEach((s) => {
                        console.log(s)
                        ss = s.metadata;
                        if (Object.keys(ss.paint)[0].split('-')[0] == 'circle') {
                            let at = {};
                            if (ss.paint['circle-stroke-width'] != undefined) {
                                at['width'] = ss.paint['circle-stroke-width']
                            };
                            if (ss.paint['circle-stroke-color'] != undefined) {
                                at['color'] = ss.paint['circle-stroke-color']
                            };

                            ci = gw.circle(16);
                            ci.fill( ss.paint['circle-color']);
                            ci.stroke({ color: at['color'], width: at['width']/2  });
                        }
                    })
                    gw.move(2,2)

                    symbol = draw.svg();
                    label = li.styles[0].id.split(':')[2];
                    label = label.slice(label.indexOf(')') + 1) //clean label parentesis
                    let entry = '<div class="legend-row"><div class="legend-symbol-left">' + symbol + '</div><div class="legend-symbol-right">' + label + '</div></div>'
                    legend.push(entry)
                    console.log(entry)
                    
                }
            }

            return legend.join('')
        },
    },


    mounted() {
        this.prepareData();

        // detect end of transition to redraw map
        let drawer = document.getElementById('drawer');
        drawer.addEventListener('transitionend', () => {
            this.resize();
        });

        window.onresize = () => this.resize();
    },


    watch: {
        atlas_menu: function () {
            setTimeout(() => {
                this.resize()
            }, 2000);
        },

        ready: function () {
            if (this.ready == true) {
                document.title = this.c_data.conf.project.name;
            }
        },
    },

    computed: {},

    created() {
        window.addEventListener('resize', this.resize);
        this.resize();
    },

    destroyed() {
        window.removeEventListener('resize', this.resize);
    },

    components: {
        // pdf: pdf
    },
})


class MenuControl {
    constructor(basemap, allBGnames, first) {
        this.basemap = basemap;
        this.first = first;
        this.allBGnames = allBGnames;
    }

    onAdd(map) {
        this.map = map;
        this.container = document.createElement('button');
        this.container.className = 'maplibregl-ctrl menu-control layer-radio';
        if (this.first) {
            this.container.className += ' active'
        };
        this.container.textContent = this.basemap.name;

        this.container.addEventListener('click', (event) => {

            let layerName = event.target.innerHTML;
            
            if (!event.target.classList.contains("active")) {
                // show basemap and hide others
                if (layerName !== 'No background')
                    map.setLayoutProperty(layerName, 'visibility', 'visible');

                for (bg in this.allBGnames) {
                    if (this.allBGnames[bg] !== layerName) {
                        map.setLayoutProperty(this.allBGnames[bg], 'visibility', 'none');
                    }
                }

                //desactivar todo
                let mm = document.querySelector('.maplibregl-ctrl.menu-control.layer-radio.active');
                mm.classList.remove("active");
            
                event.target.className +=' active'
            }
        });

        return this.container;
    }

    onRemove(){
        this.container.parentNode.removeChild(this.container);
        this.map = undefined;
    }
}