## Map viewer

Map viewer based on [Maplibre GL JS](https://maplibre.org/projects/maplibre-gl-js/) and [Vue](https://vuejs.org/).

### conf.json specificacition

All the configuration is done in a JSON file, which has to fullfill these rules (has to be completed):

#### General structure

```
{
    "project": {},
    "chapters": {
        "type": "pdf" || "atlas",
        "maps": {}
    }
}
```

#### Maps structure

```
{
    "name": string,
    "route": string,
    "desc": string,
    "properties": {},
    "map_id": string,
    "minzoom": integer,
    "maxzoom": integer,
    "layers": []
}
```

#### Layer specification

- `layerId`: *Required string.*
- `type`: *Required string `maplibre`.*
- `maplibreStyle`: *Required Maplibre Style Specification [Layer](https://maplibre.org/maplibre-gl-js-docs/style-spec/layers/).*
- `source`: *Required Maplibre Style Specificaction [Source](https://maplibre.org/maplibre-gl-js-docs/style-spec/sources/).*
- `cache`: *Optional `boolean`. Defaults to `true`.*
- `popup.popup_status`: *Optional `boolean`.*
- `legend.legend_status`: *Optional `boolean`.*
