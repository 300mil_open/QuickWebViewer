# This file contains metadata for your plugin.
[general]
name=QuickWebViewer
qgisMinimumVersion=3.22
qgisMaximumVersion=3.99
description=Publish QGIS project as web map
version=0.1
author=300.000 Km/s
email=info@300000kms.net
license=GPL-3.0-or-later

about=QuickWebViewer is QGIS Plugin for bringing your QGIS project online. Layers are exported as mbtiles and the web viewer uses maplibre.
# End of mandatory metadata

# Recommended items:

tags=web, webmaps, html, ftp, export, publishing, tiles, vector, mbtiles, maplibre, maplibre gl js

tracker=https://gitlab.com/300000-kms/QuickWebViewer/issues
repository=https://gitlab.com/300000-kms/QuickWebViewer
homepage=https://quickwebviewer.org
category=Web
icon=resources/icons/icon.png
experimental=False
deprecated=False
server=False
hasProcessingProvider=yes

# Uncomment the following line and add your changelog:
# changelog=
