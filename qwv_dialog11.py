# -*- coding: utf-8 -*-

__author__ = '(C) 2023 by Gerald Kogler'
__date__ = '1/10/2023'
__copyright__ = 'Copyright 2023, 300.000 Km/s'
__license__ = 'GPLv3 license'

import os
import math

from qgis.PyQt import uic
from qgis.PyQt import QtWidgets
from qgis.PyQt.QtWidgets import QLineEdit, QPlainTextEdit, QComboBox, QCheckBox, QSpinBox, QPushButton, QDialogButtonBox
from qgis.PyQt.QtGui import QIcon
from qgis.core import QgsProject, Qgis
from .utils import Utils

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'qwv_dialog11_layers.ui'))

MINZOOM = 8
MAXZOOM = 15
MINZOOM_ALLOWED = 1
MAXZOOM_ALLOWED = 18

class QWVDialog11(QtWidgets.QDialog, FORM_CLASS):

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(QWVDialog11, self).__init__(parent)
        # Set up the user interface from Designer through FORM_CLASS.
        # After self.setupUi() you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect

        self.iface = iface
        self.parent = parent
        self.first_start = True
        self.setupUi(self)

        # add buttons
        self.closeBtn = QPushButton(Utils.getIcon("window-close"), Utils.tr("Close"))
        self.button_box.addButton(self.closeBtn, QDialogButtonBox.ActionRole)

        self.saveBtn = QPushButton(Utils.getIcon("document-save"), Utils.tr("Save Settings"))
        self.button_box.addButton(self.saveBtn, QDialogButtonBox.ActionRole)


    def addRowLayer(self, viewName, viewLayer=None):
        """Add row to layer table."""

        index = self.dialog11_layers.rowCount()
        self.dialog11_layers.insertRow(index)

        # id
        dialog11_layers_layer = QComboBox()
        self.addLayerItems(viewName, dialog11_layers_layer, viewLayer)
        dialog11_layers_layer.currentIndexChanged.connect(lambda:self.setLayer(dialog11_layers_layer.currentText(), dialog11_layers_layer.currentData()))
        self.dialog11_layers.setCellWidget(index, 0, dialog11_layers_layer)

        # name
        dialog11_layers_name = QLineEdit()
        if viewLayer:
            dialog11_layers_name.setText(viewLayer["name"])
        self.dialog11_layers.setCellWidget(index, 1, dialog11_layers_name)

        # description
        dialog11_layers_description = QPlainTextEdit()
        if viewLayer:
            dialog11_layers_description.appendPlainText(viewLayer["description"])
        self.dialog11_layers.setCellWidget(index, 2, dialog11_layers_description)

        # legend
        dialog11_layers_legend = QCheckBox()
        if viewLayer:
            dialog11_layers_legend.setChecked(viewLayer["legend"])
        self.dialog11_layers.setCellWidget(index, 3, dialog11_layers_legend)

        #self.dialog1_layers_legend_cell = QTableWidgetItem()
        #self.dialog1_layers_legend_cell.setTextAlignment(QgsLayoutAligner.AlignHCenter)
        #self.dialog1_views.setItem(index, 3, self.dialog1_layers_legend_cell)

        # zoommin
        self.dialog11_layers.setColumnWidth(4, 0)
        dialog11_layers_zoommin = QSpinBox()
        dialog11_layers_zoommin.setMinimum(MINZOOM_ALLOWED)
        dialog11_layers_zoommin.setMaximum(MAXZOOM_ALLOWED)
        if viewLayer:
            dialog11_layers_zoommin.setValue(viewLayer["zoommin"])
        else:
            dialog11_layers_zoommin.setValue(MINZOOM)
        self.dialog11_layers.setCellWidget(index, 4, dialog11_layers_zoommin)
        dialog11_layers_zoommin.hide()

        # zoommax
        self.dialog11_layers.setColumnWidth(5, 0)
        dialog11_layers_zoommax = QSpinBox()
        dialog11_layers_zoommax.setMinimum(MINZOOM_ALLOWED)
        dialog11_layers_zoommax.setMaximum(MAXZOOM_ALLOWED)
        if viewLayer:
            dialog11_layers_zoommax.setValue(viewLayer["zoommax"])
        else:
            dialog11_layers_zoommax.setValue(MAXZOOM)
        self.dialog11_layers.setCellWidget(index, 5, dialog11_layers_zoommax)
        dialog11_layers_zoommax.hide()
            

    def removeRowLayer(self):
        """Remove row from layer table."""

        selectedRows = self.dialog11_layers.selectionModel().selectedRows()

        if len(selectedRows) > 0:
            self.dialog11_layers.removeRow(selectedRows[0].row())


    def addLayerItems(self, viewName, comboBox, viewLayer=None):
        """Add layer items to dropdown list."""

        #print("viewName:", viewName)

        comboBox.addItem("-")
        for layer in QgsProject.instance().mapThemeCollection().mapThemeVisibleLayers(viewName):
            comboBox.addItem(layer.name(), layer.id())

            if viewLayer and layer.id() == viewLayer["id"]:
                comboBox.setCurrentText(layer.name())


    def getSelectedViewRow(self):
        """Get index of selected row from layer table."""

        return self.dialog11_layers.selectionModel().selectedRows()[0].row()


    def setLayer(self, layerName, layerId):
        """Select layer and write layer name field."""

        layerNameWidget = self.dialog11_layers.cellWidget(self.getSelectedViewRow(), 1)
        if layerName != "-":
            layerNameWidget.setText(layerName)
        else:
            layerNameWidget.setText("")


    def loadData(self, viewListIndex, viewName):
        """Fill UI with layer data."""

        # print("loadData for row: ", viewListIndex, viewName)

        # remove rows from table
        self.dialog11_layers.setRowCount(0)
        self.dialog11_addlayer.disconnect()
        self.dialog11_removelayer.disconnect()

        # add rows to table
        if self.parent.dialog1_views.layers[viewListIndex] == None:
            # no layers saved for this view table row -> load all layers
            # self.addRowLayer(viewName)
            for layer in QgsProject.instance().mapThemeCollection().mapThemeVisibleLayers(viewName):

                zoommin = MINZOOM
                zoommax = MAXZOOM

                # prepopulate with Layer Scale Visibility
                if layer.hasScaleBasedVisibility():
                    zoommin = self.getZoomFromScale(layer.minimumScale())
                    zoommax = self.getZoomFromScale(layer.maximumScale())

                self.addRowLayer(viewName, {
                    "id": layer.id(),
                    "name": layer.name(),
                    "description": "",
                    "zoommin": zoommin,
                    "zoommax": zoommax,
                    "switchable": False,
                    "legend": False
                })
        else:
            # load saved data
            viewLayers = self.parent.dialog1_views.layers[viewListIndex]
            for viewLayer in viewLayers:
                self.addRowLayer(viewName, viewLayer)

        self.dialog11_addlayer.clicked.connect(lambda:self.addRowLayer(viewName))
        self.dialog11_removelayer.clicked.connect(self.removeRowLayer)


    def getZoomFromScale(self, scale):
        """Calculate web zoom from map scale factor."""

        # scale = 156543.03392804097 / math.pow(2, zoom)

        return int( math.log (156543.03392804097 / scale) / math.log(2) )


    def saveData(self):
        """Save views layers."""

        if self.uniqueLayerNames():

            #print("saveData for row:", self.parent.getSelectedViewRow())

            self.parent.dialog1_views.layers[self.parent.getSelectedViewRow()] = []
            for i in range(self.dialog11_layers.rowCount()):
                layerId = self.dialog11_layers.cellWidget(i, 0).currentData()
                layerName = self.dialog11_layers.cellWidget(i, 1).text()
                layerDescription = self.dialog11_layers.cellWidget(i, 2).toPlainText()
                layerLegend = self.dialog11_layers.cellWidget(i, 3).isChecked()
                layerZoomMin = self.dialog11_layers.cellWidget(i, 4).value()
                layerZoomMax = self.dialog11_layers.cellWidget(i, 5).value()

                #print(layerId, layerName, layerDescription, layerZoomMin, layerZoomMax, layerLegend)

                self.parent.dialog1_views.layers[self.parent.getSelectedViewRow()].append({
                    "id": layerId, 
                    "name": layerName, 
                    "description": layerDescription, 
                    "zoommin": layerZoomMin, 
                    "zoommax": layerZoomMax, 
                    "legend": layerLegend
                })

            self.hide()

        else:
            self.messageBar.pushMessage(Utils.tr("Warning"), Utils.tr("Layer names must be unique."), level=Qgis.Warning, duration=3)


    def uniqueLayerNames(self):
        """Compare all layer names to assure that they are unique."""

        layerNames = []
        for i in range(self.dialog11_layers.rowCount()):
            layerNames.append(self.dialog11_layers.cellWidget(i, 1).text())

        # print(len(layerNames), len(set(layerNames)))

        return not len(layerNames) > len(set(layerNames))