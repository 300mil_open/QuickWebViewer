FORMS = ../qwv_dialog1_base.ui ../qwv_dialog11_layers.ui ../qwv_dialog2_base.ui ../qwv_dialog3_base.ui

SOURCES = ../plugin.py ../qwv_dialog1.py ../qwv_dialog11.py ../qwv_dialog2.py ../qwv_dialog3.py ../utils.py ../map_store.py

TRANSLATIONS =  QuickWebViewer_en.ts QuickWebViewer_fr.ts QuickWebViewer_es.ts
