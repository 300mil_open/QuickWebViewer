<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Utils</name>
    <message>
        <location filename="../plugin.py" line="70"/>
        <source>QuickWebViewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="62"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="53"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="56"/>
        <source>Save Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="59"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="86"/>
        <source>Use Current Map Canvas Extent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="91"/>
        <source>Use Project Extent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="307"/>
        <source>Please open a project file in order to use this plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="359"/>
        <source>Mapset and mbtiles written successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="361"/>
        <source>You have to fill the extent in the right format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="364"/>
        <source>You have to select at least one view with one layer and the mapset name in order to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="381"/>
        <source>Viewer exported successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="383"/>
        <source>You have to define the Mapset Folder and select one mapset to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="403"/>
        <source>You have to define at least a Mapset name (dialog1) and the Public URL and Path to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="407"/>
        <source>Viewer published successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="71"/>
        <source>OpenStreetMap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="74"/>
        <source>Satellite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="232"/>
        <source>You have to select a view in order to edit its layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="466"/>
        <source>Could not remove GPKG </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11.py" line="223"/>
        <source>Layer names must be unique.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2.py" line="186"/>
        <source>mbtiles file does not exist: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="85"/>
        <source>You have to define local and remote folder in Project Settings in order to use FTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="108"/>
        <source>You have to define Host, User and Password in Project Settings in order to use SFTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="126"/>
        <source>Could not connect to SFTP server {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="154"/>
        <source>SFTP connection to {} established successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="189"/>
        <source>Could not connect to FTP server {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="195"/>
        <source>Login failed for user {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="211"/>
        <source>FTP connection to {} established successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../map_store.py" line="79"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../map_store.py" line="243"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="239"/>
        <source>1. Export mapset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="244"/>
        <source>2. Export viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="249"/>
        <source>3. Publish viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../map_store.py" line="79"/>
        <source>Settings from old plugin version detected, please delete &apos;project_qwv&apos; from Project Settings -&gt; Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../map_store.py" line="243"/>
        <source>Settings written to QGIS project. In order to have them available in the future, you have to save your QGIS project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="188"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qwvDialogBase1</name>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="20"/>
        <source>QuickWebViewer - 1. Export mapset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="54"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="113"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="138"/>
        <source>Export folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="118"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="201"/>
        <source>Backgrounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="74"/>
        <source>Attached Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="230"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="245"/>
        <source>Add another file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="256"/>
        <source>Remove selected file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="194"/>
        <source>Extent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="294"/>
        <source>Select views to export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="340"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="355"/>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="123"/>
        <source>Zoom min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="128"/>
        <source>Zoom max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="375"/>
        <source>Add another view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="386"/>
        <source>Remove selected view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="427"/>
        <source>Export every view as separate mbtile instead of all views into one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="14"/>
        <source>QuickWebViewer - Select layers to export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="29"/>
        <source>Select layers to export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="37"/>
        <source>Add new field pair as part of a composite foreign key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="48"/>
        <source>Remove the selected or last pair of fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="108"/>
        <source>Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="133"/>
        <source>Legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="157"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qwvDialogBase2</name>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="14"/>
        <source>QuickWebViewer - 2. Export Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="30"/>
        <source>Mapsets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="36"/>
        <source>1. Select Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="43"/>
        <source>2. Select Mapsets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="60"/>
        <source>Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="66"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="76"/>
        <source>Subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="86"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="96"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="113"/>
        <source>Output folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="136"/>
        <source>Logos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="155"/>
        <source>Image file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="239"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="165"/>
        <source>URL Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="175"/>
        <source>Remove another logo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="186"/>
        <source>Remove selected layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="215"/>
        <source>Attached Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="234"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="249"/>
        <source>Add another file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="260"/>
        <source>Remove selected file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="126"/>
        <source>Full Description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qwvDialogBase3</name>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="14"/>
        <source>QuickWebViewer - 3. Publish Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="30"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="36"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="53"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="59"/>
        <source>Protocol:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="67"/>
        <source>FTP - File Transfer Protocol (port 21)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="72"/>
        <source>SFTP - SSH File Transfer Protocol (port 22)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="80"/>
        <source>Host:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="90"/>
        <source>Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="100"/>
        <source>User:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="110"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="126"/>
        <source>Test Connection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
