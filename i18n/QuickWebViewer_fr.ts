<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>Utils</name>
    <message>
        <location filename="../plugin.py" line="70"/>
        <source>QuickWebViewer</source>
        <translation>QuickWebViewer</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="239"/>
        <source>1. Export mapset</source>
        <translation>1. Exporter mapset</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="244"/>
        <source>2. Export viewer</source>
        <translation>2. Exporter viewer</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="249"/>
        <source>3. Publish viewer</source>
        <translation>3. Publier viewer</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="62"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="53"/>
        <source>Run</source>
        <translation>Exécuter</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="56"/>
        <source>Save Settings</source>
        <translation>Enregistrer les paramètres</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="59"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="86"/>
        <source>Use Current Map Canvas Extent</source>
        <translation>Utiliser l&apos;étendue de la carte actuelle</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="91"/>
        <source>Use Project Extent</source>
        <translation>Utiliser l&apos;étendue du projet</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="307"/>
        <source>Please open a project file in order to use this plugin</source>
        <translation>Ouvrir un fichier de projet pour utiliser l&apos;extension</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="359"/>
        <source>Mapset and mbtiles written successfully.</source>
        <translation>Mapset et mbtiles écrits avec succès.</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="361"/>
        <source>You have to fill the extent in the right format.</source>
        <translation>Il faut remplir l&apos;espace dans le format approprié.</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="364"/>
        <source>You have to select at least one view with one layer and the mapset name in order to proceed.</source>
        <translation>Il faut sélectionner au moins une thème avec une couche et le nom du mapset pour pouvoir continuer.</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="381"/>
        <source>Viewer exported successfully.</source>
        <translation>Le viewer a été exporté avec succès.</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="383"/>
        <source>You have to define the Mapset Folder and select one mapset to proceed.</source>
        <translation>Il faut définir le dossier des mapsets et sélectionner un mapset pour continuer.</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="403"/>
        <source>You have to define at least a Mapset name (dialog1) and the Public URL and Path to proceed.</source>
        <translation>Il faut au moins définir un nom de Mapset (dialog1) ainsi que l&apos;URL publique et le chemin d&apos;accès pour continuer.</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="407"/>
        <source>Viewer published successfully.</source>
        <translation>Le viewer a été publié avec succès.</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="71"/>
        <source>OpenStreetMap</source>
        <translation>OpenStreetMap</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="74"/>
        <source>Satellite</source>
        <translation>Satellite</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="232"/>
        <source>You have to select a view in order to edit its layers</source>
        <translation>Il est nécessaire de sélectionner une thème pour modifier ses couches</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="466"/>
        <source>Could not remove GPKG </source>
        <translation>Impossible de supprimer GPKG </translation>
    </message>
    <message>
        <location filename="../qwv_dialog11.py" line="223"/>
        <source>Layer names must be unique.</source>
        <translation>Les noms de couches doivent être uniques.</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2.py" line="186"/>
        <source>mbtiles file does not exist: </source>
        <translation>Le fichier mbtiles n&apos;existe pas: </translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="85"/>
        <source>You have to define local and remote folder in Project Settings in order to use FTP</source>
        <translation>Il est nécessaire de définir un dossier local et un dossier distant dans les paramètres du projet afin d&apos;utiliser le protocole FTP</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="108"/>
        <source>You have to define Host, User and Password in Project Settings in order to use SFTP</source>
        <translation>Il est nécessaire de définir l&apos;Host, l&apos;Utilisateur et le mot de passe dans les paramètres du projet afin d&apos;utiliser SFTP</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="126"/>
        <source>Could not connect to SFTP server {}</source>
        <translation>Impossible de se connecter au serveur SFTP {}</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="154"/>
        <source>SFTP connection to {} established successfully</source>
        <translation>Connexion SFTP à {} établie avec succès</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="189"/>
        <source>Could not connect to FTP server {}</source>
        <translation>Impossible de se connecter au serveur FTP {}</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="195"/>
        <source>Login failed for user {}</source>
        <translation>La connexion a échoué pour l&apos;utilisateur {}</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3.py" line="211"/>
        <source>FTP connection to {} established successfully</source>
        <translation>La connexion FTP avec {} a été établie avec succès</translation>
    </message>
    <message>
        <location filename="../map_store.py" line="79"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../map_store.py" line="243"/>
        <source>Success</source>
        <translation>Succès</translation>
    </message>
    <message>
        <location filename="../map_store.py" line="79"/>
        <source>Settings from old plugin version detected, please delete &apos;project_qwv&apos; from Project Settings -&gt; Variables</source>
        <translation>Paramètres d&apos;une précédente version de l&apos;extension détectés, veuillez supprimer &apos;project_qwv&apos; dans Paramètres du projet -&amp;gt; Variables</translation>
    </message>
    <message>
        <location filename="../map_store.py" line="243"/>
        <source>Settings written to QGIS project. In order to have them available in the future, you have to save your QGIS project.</source>
        <translation>Paramètres écrits dans le projet QGIS. Pour qu&apos;ils soient disponibles à l&apos;avenir, il faut sauvegarder le projet QGIS.</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1.py" line="188"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
</context>
<context>
    <name>qwvDialogBase1</name>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="20"/>
        <source>QuickWebViewer - 1. Export mapset</source>
        <translation>QuickWebViewer - 1. Export mapset</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="54"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="113"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="138"/>
        <source>Export folder</source>
        <translation>Dossier d&apos;exportation</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="118"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="201"/>
        <source>Backgrounds</source>
        <translation>Fond</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="74"/>
        <source>Attached Files</source>
        <translation>Fichiers joints</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="230"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="245"/>
        <source>Add another file</source>
        <translation>Ajouter un autre fichier</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="256"/>
        <source>Remove selected file</source>
        <translation>Supprimer les fichiers sélectionnés</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="194"/>
        <source>Extent</source>
        <translation>Étendue</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="294"/>
        <source>Select views to export</source>
        <translation>Sélection les thèmes à exporter</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="340"/>
        <source>View</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="355"/>
        <source>Layers</source>
        <translation>Couches</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="123"/>
        <source>Zoom min</source>
        <translation>Zoom min</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="128"/>
        <source>Zoom max</source>
        <translation>Zoom max</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="375"/>
        <source>Add another view</source>
        <translation>Ajouter une autre thème</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="386"/>
        <source>Remove selected view</source>
        <translation>Supprimer le thème sélectionnée</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="427"/>
        <source>Export every view as separate mbtile instead of all views into one</source>
        <translation>Export chaque thème en tant que mbtile séparé au lieu de toutes les thèmes en une seule</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="14"/>
        <source>QuickWebViewer - Select layers to export</source>
        <translation>QuickWebViewer - Sélection des couches à exporter</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="29"/>
        <source>Select layers to export</source>
        <translation>Sélection des couches à exporter</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="37"/>
        <source>Add new field pair as part of a composite foreign key</source>
        <translation>Ajouter une nouvelle paire de champs dans le cadre d&apos;une clé étrangère composite</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="48"/>
        <source>Remove the selected or last pair of fields</source>
        <translation>Supprimer la paire de fields sélectionnée ou la dernière paire de fields</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="108"/>
        <source>Layer</source>
        <translation>Couche</translation>
    </message>
    <message>
        <location filename="../qwv_dialog11_layers.ui" line="133"/>
        <source>Legend</source>
        <translation>Légende</translation>
    </message>
    <message>
        <location filename="../qwv_dialog1_base.ui" line="157"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>qwvDialogBase2</name>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="14"/>
        <source>QuickWebViewer - 2. Export Viewer</source>
        <translation>QuickWebViewer - 2. Export Viewer</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="30"/>
        <source>Mapsets</source>
        <translation>Mapsets</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="36"/>
        <source>1. Select Folder</source>
        <translation>1. Sélection le dossier</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="43"/>
        <source>2. Select Mapsets</source>
        <translation>2. Sélection Mapset</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="60"/>
        <source>Viewer</source>
        <translation>Viewer</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="66"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="76"/>
        <source>Subtitle</source>
        <translation>Sous-titre</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="86"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="96"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="113"/>
        <source>Output folder</source>
        <translation>Dossier de sortie</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="136"/>
        <source>Logos</source>
        <translation>Logos</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="155"/>
        <source>Image file</source>
        <translation>Fichier image</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="239"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="165"/>
        <source>URL Link</source>
        <translation>URL Lien</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="175"/>
        <source>Remove another logo</source>
        <translation>Supprimer un autre logo</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="186"/>
        <source>Remove selected layer</source>
        <translation>Supprimer la couche sélectionnée</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="215"/>
        <source>Attached Files</source>
        <translation>Fichiers joints</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="234"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="249"/>
        <source>Add another file</source>
        <translation>Ajouter un autre fichier</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="260"/>
        <source>Remove selected file</source>
        <translation>Supprimer le fichier sélectionné</translation>
    </message>
    <message>
        <location filename="../qwv_dialog2_base.ui" line="126"/>
        <source>Full Description</source>
        <translation>Description Complète</translation>
    </message>
</context>
<context>
    <name>qwvDialogBase3</name>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="14"/>
        <source>QuickWebViewer - 3. Publish Viewer</source>
        <translation>QuickWebViewer - 3. Publier Viewer</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="30"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="36"/>
        <source>Folder</source>
        <translation>Dossier</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="53"/>
        <source>Server</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="59"/>
        <source>Protocol:</source>
        <translation>Protocol :</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="67"/>
        <source>FTP - File Transfer Protocol (port 21)</source>
        <translation>FTP - File Transfer Protocol (port 21)</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="72"/>
        <source>SFTP - SSH File Transfer Protocol (port 22)</source>
        <translation>SFTP - SSH File Transfer Protocol (port 22)</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="80"/>
        <source>Host:</source>
        <translation>Host:</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="90"/>
        <source>Path:</source>
        <translation>Chemin d&apos;accès (Path) :</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="100"/>
        <source>User:</source>
        <translation>Utilisateur :</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="110"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <location filename="../qwv_dialog3_base.ui" line="126"/>
        <source>Test Connection</source>
        <translation>Test de connexion</translation>
    </message>
</context>
</TS>
