<span style="color: #000000; font-family: Montserrat; font-size: 16px;">


# ![logo](readme_imgs/logo2.svg)



# QuickWebViewer
    
## A QGIS plugin to easily publish your maps online

    
***
    
    
### Installation
    
The QuickWebViewer plugin can be downloaded and installed via the .zip folder, accessible at the following link:

- 🔗 [QuickWebViewer](http://www.quickwebviewer.org/)   
    
  
    
***    
    
    
    
### ❓ What is QuickWebViewer

The QuickWebViewer is a plugin that enables the online publication of maps created in QGIS through a simple three step process. It renders the maps visible in the form of a dynamic and interactive platform.


    

***
    
    
    
    
### ❓ How it works
    
- #### Merge different files:
  The plugin enables the merging of various files into a single online viewer.

- #### Map-based views:
  Each view in QGIS is presented as a map.

- #### Upload to low-cost managed server:
  Users can easily upload their maps to a cost-effective managed server.

- #### File management:
  Effortlessly manage and visualize files through the QGIS interface.

- #### Upcoming feature: raster compatibility
  Future updates will include raster data visualization.
    
    
    
***
    


### ❓ Why you need it
    
    
    
    
    
- #### Easily online map publication:
 
Streamline the process of publishing QGIS-created maps online with a user-friendly three-step mechanism.

- #### Dynamic and interactive visualization:
 
Elevate your maps by rendering them in a dynamic and interactive platform, providing a richer experience for     users.

- #### Simplified workflow:

Follow a three-step process for online map publication, reducing complexity and saving time.

- #### Optimized map visibility:

Ensure maps are presented with clarity and precision, optimizing visibility for effective communication of geographic data.

- #### Facilitates collaboration:

Enhance collaboration by offering a convenient online platform for sharing and interacting with QGIS maps.
    

***
    
    
### ❓ Why you not need it 
    
    
    
    
    
- #### Offline map usage preference:
    
If your mapping activities primarily involve offline scenarios, and online publication isn't a priority.
- #### Simple visualization requirements:

If your mapping requirements are adequately met with static visualizations, and you don't require dynamic or interactive platforms.
    
- #### Existing comfort with alternatives:
    
If you are already comfortable with alternative methods or tools for online map publication and find them effective.

- #### Effective external file management:
    
If your current file management systems outside of QGIS are well-established and meet your needs, the additional file management features of the plugin might not be a top priority.

- #### Simplified collaboration practices:
    
If your current collaboration methods are simple, effective, and don't require the additional collaborative features provided by the plugin.
    

***
    
    
### Before to publish
    
    
    
    

Before proceeding with the three parts of the plugin, it's crucial to ensure that the folder structure on your computer is correct. 
This ensures the proper execution of exports and guarantees the functionality of the QuickWebViewer plugin.
    

- Create a folder (e.g., "projectname_export") that will serve as the reference directory where mapset (Step 1) and viewer (Step 2) folders will be generated by the plugin.
    
    
    
- In all steps, the "Save Settings" option allows you to keep entered data saved and retrieve them in desired steps. Always execute "Save Setting" before running the "Run".

***


### Publish in three steps
    
    
    
    
<img align="left" width="30" height="30" src="readme_imgs/icon_1.svg"> &nbsp;&nbsp;&nbsp; Step_1 : Export mapset
    
<img align="left" width="30" height="30" src="readme_imgs/icon_2.svg"> &nbsp;&nbsp;&nbsp; Step_2 : Export viewer 
    
<img align="left" width="30" height="30" src="readme_imgs/icon_3.svg"> &nbsp;&nbsp;&nbsp; Step_3 : Publish viewer 
    
    
The correct performance of the plugin is ensured through the correct execution of three steps. The various stages of each step will be explained in detail below.    
    

    
***
    
 
## Step_1 : Export mapset
<img align="left" width="50" height="50" src="readme_imgs/icon_1.svg">  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        
    

    
    
    
    

![step1](readme_imgs/rm_1.png) 

#### Details:

1. Name: Enter the mapset name.

2. Export folders: Specify the reference path to the previously created folder (e.g., "projectname_export") where the mapset folder will be generated.

3. Description: Add a description related to the mapset.

4. Backgrounds: Select background maps for overlaying cartographic data. Adjust visibility after viewer publication.

5. Attached files: Attach PDF documents with reference names, downloadable in the final viewer.

6. Extent: Specify the export area using either:
   - Use Current Map Canvas Extent
   - Use Project Extent

#### Select views to export:

7. View: Choose a theme from various views created in the QGIS file.

8. Name: Customize the name of each view.
    
9. Description: Add a brief description for each view.
    
10. Layers: Modify individual layers within the view. Click "Edit" to customize layer settings.

![step1](readme_imgs/rm_2.png) 

10b. click ---> Edit ---> customize each layer:

- Layer: Name of the layer.
- Name: Editable layer name.
- Description: Short specific description.
- Legend: Enable or disable visibility in the legend.

11. Zoom min: Set minimum display zoom.

12. Zoom max: Set maximum display zoom.

13. It is recommended but not mandatory to enable it when exporting multiple views. 
    It is not necessary when exporting a single view. 

*** 
    
    
    
## Step_2 : Export viewer
<img align="left" width="50" height="50" src="readme_imgs/icon_2.svg"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
    
    

![step1](readme_imgs/rm_3.png) 

#### Mapset:

1. Select Folder: Choose the folder generated in STEP 1.
    
2. Select Mapsets: Choose one or more mapset sets for conversion based on your needs.

#### Viewer:
    
3. Title: Define the title in the viewer.

4. Subtitle: Add a supporting subtitle in the viewer.

5. Description: Insert a brief description in the viewer.

6. Full description: Provide a more detailed and complete descriptive text.

7. Attribution : Insert a text referring to the attribution of the project and in terms of a disclaimer. 

8. Output folder: Indicate the viewer's save direction referring to the reference folder (e.g."projectname_export") where mapset and viewer were generated in Step 1.

#### Logos:
    
9. 
- Image file: Define the logo image path.
- Name: Add the logo name for display.
- URL link: Include a URL link for a clickable logo directing to a website.

#### Attached files:
    
10. 
- File: Define the document location path.
- Name: Add a specific name for documents in the viewer.
    
    

***
    
    
    
## Step_3 : Publish viewer 
<img align="left" width="50" height="50" src="readme_imgs/icon_3.svg"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

   

    
    
    
    
![step3](readme_imgs/rm_4.png) 

#### Project:

1. Folder: Select the path where the "viewer" was exported in STEP 2. 

- Note: The structure and organisation of folders are crucial for the functionality of plugins.

#### Server:

2. Protocol: FTP - File Transfer Protocol (port 21) 
    
3. Host:

4. Path: Optional; assign a name for different versions (e.g., v1, v2, v3).

5. User: Define a username.

6. Password: Set a reference password for your user.

Due to stricter security standards, this step might not work on some computers. 

In such cases, an alternative using FileZilla is available: 🔗 [FileZilla](https://filezilla-project.org/)
    
    
    
***
   
    
## QGIS Expressions accepted for styling

| Function    | Description                            |
|-------------|:--------------------------------------:|
| `acos`      | Arc cosine function                    |
| `asin`      | Arc sine function                      |
| `atan`      | Arc tangent function                   |
| `atan2`     | Arc tangent of two variables           |
| `ceil`      | Ceiling function (round up)            |
| `concat`    | Concatenate strings                    |
| `cos`       | Cosine function                        |
| `degrees`   | Convert radians to degrees             |
| `floor`     | Floor function (round down)            |
| `if`        | Conditional statement                  |
| `lower`     | Convert string to lowercase            |
| `ln`        | Natural logarithm                      |
| `max`       | Maximum value of two numbers           |
| `min`       | Minimum value of two numbers           |
| `radians`   | Convert degrees to radians             |
| `replace`   | Replace substring in a string          |
| `sin`       | Sine function                          |
| `substr`    | Extract a substring                    |
| `tan`       | Tangent function                       |
| `title`     | Convert string to title case           |
| `to_int`    | Convert to integer                     |
| `to_float`  | Convert to floating-point number       |
| `to_string` | Convert to string                      |
    
*** 



   

     
## Good tips 

    
***


✅ The unit of measurement to be used in the layer symbology is metres at scale.

✅ The viewer can be consulted via PC and via mobile phone
    
    
✅ vector layers/data
    
❌ raster layers/data



#### expression

✅ concat("A", "B")


❌ "A" + "B"   


In the case where we want to combine two columns for example column "type" and column "status"


✅  concat("type", "status")

❌ "type" + "status"   


#### labels 
    

✅ The unit of measurement to be used in the definition of labels is millimetres.

✅ single labels 
    
✅ ruled-based labeling

❌ formatting space letter 
    
✅ wrap on characters 
    
✅ buffer
    
✅ draw background

❌ text style "italic" 

   


#### symbology
    
✅ single symbol
    
✅ categorized
    
✅ graduated
    
✅ ruled-based
    
✅ symbol layer type : simple fill, outline : simple line 
    
❌ heatmap style 
    
✅ svg qgis marker symbology

✅ dot-line 

✅ geometry type layer : polygon, line, point 
    
✅ always use 'no brush' and insert colour in simple fill is a necessary condition if you only want to display the polygon outline
    
✅ accept in fill style : solid, no brush
❌ no accept in fill style : horizontal, vertical, cross, diagonal, dense
    
✅ set as symbol layer type : "outline : simple line" to represent the dot outline of a polygon (geometry type) without filling. 

*** 

     
## Conversion table

 
***
    
     
For the use of the conversion table, please refer to the calculation used in bridge style, for more information : 

🔗 [GeoCat](https://github.com/GeoCat/bridge-style/blob/master/bridgestyle/mapboxgl/fromgeostyler.py#L132)
***
We also refer to the documentation as support:

Regarding the number 279581257 Mapbox documentation which is well summarised:

🔗 [Mapbox](https://docs.mapbox.com/help/glossary/zoom-level/)
***
    
For scales, reference is to this document : 
    
🔗 [OSM](https://wiki.openstreetmap.org/wiki/Zoom_levels)
***    
         
    
*** 

     
## Server 

 
***
    
### ❓ How does online publication work 
    
    
#### Prerequisites:

- A managed server with PHP and PDO_SQLite support.
   This includes managed servers such as:
    - 🔗 [Ionos](https://www.ionos.es/)
    - 🔗 [OVH](https://www.ovhcloud.com/es-es/)

#### Installation steps:

1. If you have FTP access:
   - Simply use the 3D plugin button.

2. If you are using SFTP access:
   - Utilize FileZilla for seamless file transfer.

#### Note:

- In case your connection is slow or experiences interruptions:
  - It is recommended to use FileZilla for a smoother experience.    

    
      
#### ❓ How upload with the plugin
    
***
    
 <img align="center" width="600" height="600" src="step3_readme_edit.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
    
1. Project : select the export path in the "viewer" folder

It is essential to note that the backup structure and the organisation of the folders are of fundamental importance to the correct operation of the extension.

2. Server : 
    
Protocol: FTP - SSH File Transfer Protocol (port 21) 
    
    
Host: define Host
    
    
Path: this does not have to be defined, you can assign a name to create different versions (such as v1, v2, v3 for example)
    
    
User: define User 
   
    
Password: set a reference password for your user.
    
    
#### 📌 extra support information

Click on "Save settings" and then "Run".

When everything is complete, you can run a "Connection test" before proceeding with the final export.


    
#### ❓ How to upload with filezilla 
    
🔗 [FileZilla](https://filezilla-project.org/) 
***
    
 <img align="center" width="800" height="800" src="rreadme_imgs/eadme_fz_edit.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
    
    
1. toolbar
    
    
2. quickconnect bar
    
    
3. message log : displays messages related to the transfer and connection, below which you will find the file listing
    
    
4. displays the local files and directories, i.e. the objects on the PC that are being used in FileZilla
    
    
5. displays the files and directories found on the server to which you are connected
    
    
6. transfer queue : which lists the files that will start to be transferred or have already been transferred
    

    
#### 📌 extra support information

4. AND 5.
use a directory tree as a header and also show a detailed listing of the directory that is selected. 
You can easily navigate through the directories by clicking on them with the mouse cursor, as in any file manager.   
    
    
    
    
### Demo viewer 
    
    
### Credits / Author
    
    
    
</span>



*** 
